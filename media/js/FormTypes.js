/*This is our input types for desktop environments*/
var FormTypesDesktop_ = {

    /*
     * This form type is used to create a simple text box.
     * Usage is: { "name" : "name of input" , "value" : "inital vale" , "type" : "text" }
     * This will create a text box with the specified inital value , the inital value is not required
    */
    "text" : function text( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','text')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
    },
    /*
     * This form type is used to create a simple password box.
     * Usage is: { "name" : "name of input" , "value" : "inital value" , "type" : "password" }
     * This will create a password box with the specified inital value , the inital value is not required
    */
    "password" : function text( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','password')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
    },
    /*
     * This form type is used to create a simple date box.
     * Usage is: { "name" : "name of input" , "value" : "DD-MM-YYYY" , "type" : "date" }
     * This will create a date box with the specified inital value , the inital value is not required
    */
    "date" : function date( name , value ) {
        return $('<div/>').addClass('input-append date input-group').attr('data-date', (value===undefined) ? moment().format('DD-MM-YYYY') : value ).attr('data-date-format','dd-mm-yyyy').append(
                /*This will add our read only text box to store the date*/
                $('<input/>').attr('name' , name ).addClass('form-control input-sm').attr('type','text').attr('readonly','').attr('value', (value===undefined) ? moment().format('DD-MM-YYYY') : value ),
                /*This will store our date picker button*/
                $('<span/>').addClass('input-group-btn').append( 
                    $('<button/>').addClass('btn btn-default btn-sm').attr('type','button') .append( $('<i/>').addClass('glyphicon glyphicon-calendar') ) )
            ).datepicker();
    },
    /*
     * This form type is used to create a simple file box.
     * Usage is: { "name" : "name of input" , "type" : "file" }
     * This will create a file box
    */
    "file" : function file ( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','file')
                .addClass("col-md-10 filestyle").attr('data-size' , 'sm' );
    },
    /*
     * This function is used create a colour input
     * Usages is { "name" : "name of input " , "type" : "color" }
    */
    "color" : function color ( name , value ) {
        return $('<div/>').addClass('input-group').append(
                    $('<input/>').attr('type','text').attr('name',name).attr('value', (value===undefined) ? '#0000' : value ).addClass('form-control input-sm') ,
                    $('<span/>').addClass('input-group-btn').append( 
                        $('<button/>').addClass('btn btn-default btn-sm').attr('type','button') .append(
                            $('<i/>').addClass('glyphicon glyphicon-none')
                        ))
                ).colorpicker();
    },
    /*
     * This function is used create a time input
     * Usages is { "name" : "name of input " , "type" : "time" }
    */
    "time" : function time ( name , value ) {
        return $('<div/>').addClass('input-group bootstrap-timepicker').append(
                    $('<input/>').attr('name',name).attr('type','text').attr('value', (value===undefined) ? moment().format('hh:mm A') : value ).addClass('form-control input-sm') ,
                    $('<span/>').addClass('input-group-btn').append(
                        $('<button/>').addClass('btn btn-default btn-sm').attr('type','button').append( 
                            $('<i/>').addClass('glyphicon glyphicon-time') 

                        ))
                ).timepicker();
    },
    /*
     * This function is used for rendering a dropdown menu
     * Usages is { "name" : "name of input " , "type" : "dropdown" , "data" : [ {  "value" : "One" , "text" : "The first" } ] }
    */
    "dropdown" : function dropdown ( name , value , data ) {
        var select_ = $('<select/>').addClass('form-control input-sm').attr('name',name);
        /*Data should be a list of { 'value' : 'val' , 'text' : 'my text' } pairs*/
        for( var i = 0 ; i < data.length ; i++ ){
            if ( value != data[i].value ){
                select_.append( $('<option/>').attr('value',data[i].value).html(data[i].text) );
            }else{
                select_.append( $('<option/>').attr('value',data[i].value).attr('selected','').html(data[i].text) );
            }
        }
        return select_;
    },
    /*
     * This function is used for rendering a checkbox.
     * Usage is { "name" : "name of input" , "value" : "starting value" ,  "type" : "color" }
    */
    "checkbox" : function file ( name , value ) {
        /*Return our nice little text box*/
        var check_box_ = $('<input/>').attr('name' , name ).attr('id',name )
                    .attr('type','checkbox').addClass("css-checkbox");
        if ( value == true ){
            check_box_.attr('checked','');
        }

        return $('<span/>').append( check_box_ , $('<label/>').addClass('css-label').attr('for', name ) );
    },
    /*
     * This form type is used to create a selection table.
     *
    */
    "table-select" : function table ( name , value , data  , selected ){

        if ( selected.values === undefined )
            selected.values = [];

        var table_ = $('<table/>').attr('id','DTT_' + name + '_').addClass("table table-striped table-selectable");
        var table_th_ = $('<thead></thead>');
        var table_tr_ = $('<tr/>');
        var aoColumns_ = [];

        for ( var i = 0 ; i < data.length ; i ++ ){
            table_tr_.append( $('<th/>').html( (data[i].text==undefined) ? '' : data[i].text ));
            aoColumns_.push( { 'mData' : data[i].mData } );
        }


        /*If we dont have our selected paramter set just return*/
        if ( selected == undefined || selected['idProp'] == undefined || selected['values'] == undefined )
            throw "Selected idProp should be defined!";

        var data_table_ = table_.append(table_th_.append(table_tr_)).dataTable( {
            "aoColumns" : aoColumns_,
            "aaData" : value,
            "fnCreatedRow" : function( nRow, aData, iDataIndex ) {

                var _input_ = $('<input/>').css('display','none').attr('name',name+'[]').attr('type','text').attr('value' , aData[selected.idProp]);

                if ( jQuery.inArray ( aData[selected.idProp] , selected.values ) != -1 ){
                    $(nRow).addClass('selected');
                }else{
                    _input_.attr('disabled','true');
                }

                $(nRow).append( _input_ );
            }
        });

        var table_api_ = data_table_.api();

        /*This is used to handle the on click of our table*/
        table_.on( 'click' , 'tr' , function(){
            if( $(this).has('th').length == 0 ){
                $(this).toggleClass('selected');

                if ( $(this).children('input').attr('disabled') === undefined ){
                    $(this).children('input').attr('disabled','true');
                }else{
                    $(this).children('input').removeAttr('disabled');
                }
            }
        });

        return data_table_;
    },
    "table-edit" : function table ( name , value , data ){

        var table_ = $('<table/>').attr('id','DTT_' + name + '_').addClass("table table-striped table-bordered");
        var table_th_ = $('<thead></thead>');
        var table_tr_ = $('<tr/>');
        var aoColumns_ = [];
        var input_ = $('<input/>').attr('type','text').attr('name',name)
                        .css('display','none').attr('value', JSON.stringify(value));

        for ( var i = 0 ; i < data.length ; i ++ ){
            table_tr_.append( $('<th/>').html( (data[i].text==undefined) ? '' : data[i].text ));
            aoColumns_.push( { 'mData' : data[i].mData } );
        }

        table_.on( 'click' , 'tr' , function(){
            if ( $(this).hasClass('info') ){
                $(this).removeClass('info');
            }else{
                if( $(this).has('th').length == 0 )
                    $(this).addClass('info');
            }
        });

        var data_table_ = table_.append(table_th_.append(table_tr_)).dataTable( {
            "columnDefs" : data,
            "aoColumns" : aoColumns_,
            "aaData" : value
        }).append( input_ );

        table_.on( 'update-table-' + name , function(e){
            //data_table_
        });

        return data_table_;
    }
}










/*
 * This is our input types for mobile environments.
 * All of the 
*/
var FormTypesMobile_ = {

    /*
     * This form type is used to create a simple text box.
     * Usage is: { "name" : "name of input" , "value" : "inital vale" , "type" : "text" }
     * This will create a text box with the specified inital value , the inital value is not required
    */
    "text" : function text( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','text')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
    },
    /*
     * This form type is used to create a simple password box.
     * Usage is: { "name" : "name of input" , "value" : "inital value" , "type" : "password" }
     * This will create a password box with the specified inital value , the inital value is not required
    */
    "password" : function text( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','password')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
    },
    /*
     * This form type is used to create a simple date box.
     * Usage is: { "name" : "name of input" , "value" : "DD-MM-YYYY" , "type" : "date" }
     * This will create a date box with the specified inital value , the inital value is not required
    */
    "date" : function date( name , value ) {
        /*Return our nice little date box*/
        return $('<input/>').attr('name' , name ).attr('type','date')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
    },
    /*
     * This form type is used to create a simple file box.
     * Usage is: { "name" : "name of input" , "type" : "file" }
     * This will create a file box
    */
    "file" : function file ( name , value ) {
        /*Return our nice little text box*/
        return $('<input/>').attr('name' , name ).attr('type','file')
                .addClass("col-md-10 filestyle").attr('data-size' , 'sm' );
    },
    /*
     * This function is used create a colour input
     * Usages is { "name" : "name of input " , "type" : "color" }
    */
    "color" : function color ( name , value ) {
        return $('<div/>').addClass('input-group').append(
                    $('<input/>').attr('type','text').attr('name',name).attr('value', (value===undefined) ? '#0000' : value ).addClass('form-control input-sm') ,
                    $('<span/>').addClass('input-group-btn').append( 
                        $('<button/>').addClass('btn btn-default btn-sm').attr('type','button') .append(
                            $('<i/>').addClass('glyphicon glyphicon-none')
                        ))
                ).colorpicker();
    },
    /*
     * This function is used create a time input
     * Usages is { "name" : "name of input " , "type" : "time" }
    */
    "time" : function time ( name , value ) {
        /*Return our nice little time box*/
        return $('<input/>').attr('name' , name ).attr('type','time')
                .addClass("col-md-10 form-control input-sm").attr('value' , (value==undefined) ? '' : value );
   
    },
    /*
     * This function is used for rendering a dropdown menu
     * Usages is { "name" : "name of input " , "type" : "dropdown" , "data" : [ {  "value" : "One" , "text" : "The first" } ] }
    */
    "dropdown" : function dropdown ( name , value , data ) {
        var select_ = $('<select/>').addClass('form-control input-sm').attr('name',name);
        /*Data should be a list of { 'value' : 'val' , 'text' : 'my text' } pairs*/
        for( var i = 0 ; i < data.length ; i++ ){
            if ( value != data[i].value ){
                select_.append( $('<option/>').attr('value',data[i].value).html(data[i].text) );
            }else{
                select_.append( $('<option/>').attr('value',data[i].value).attr('selected','').html(data[i].text) );
            }
        }
        return select_;
    },
    /*
     * This function is used for rendering a checkbox.
     * Usage is { "name" : "name of input" , "value" : "starting value" ,  "type" : "color" }
    */
    "checkbox" : function file ( name , value ) {
        /*Return our nice little text box*/
        var check_box_ = $('<input/>').attr('name' , name ).attr('id',name )
                    .attr('type','checkbox').addClass("css-checkbox");
        if ( value == true ){
            check_box_.attr('checked','');
        }

        return $('<span/>').append( check_box_ , $('<label/>').addClass('css-label').attr('for', name ) );
    },
    /*
     * This form type is used to create a selection table.
     *
    */
    "table-select" : function table ( name , value , data  , selected ){

        if ( selected.values === undefined )
            selected.values = [];

        var table_ = $('<table/>').attr('id','DTT_' + name + '_').addClass("table table-striped table-selectable");
        var table_th_ = $('<thead></thead>');
        var table_tr_ = $('<tr/>');
        var aoColumns_ = [];

        for ( var i = 0 ; i < data.length ; i ++ ){
            table_tr_.append( $('<th/>').html( (data[i].text==undefined) ? '' : data[i].text ));
            aoColumns_.push( { 'mData' : data[i].mData } );
        }


        /*If we dont have our selected paramter set just return*/
        if ( selected == undefined || selected['idProp'] == undefined || selected['values'] == undefined )
            throw "Selected idProp should be defined!";

        var data_table_ = table_.append(table_th_.append(table_tr_)).dataTable( {
            "aoColumns" : aoColumns_,
            "aaData" : value,
            "fnCreatedRow" : function( nRow, aData, iDataIndex ) {

                var _input_ = $('<input/>').css('display','none').attr('name',name+'[]').attr('type','text').attr('value' , aData[selected.idProp]);

                if ( jQuery.inArray ( aData[selected.idProp] , selected.values ) != -1 ){
                    $(nRow).addClass('selected');
                }else{
                    _input_.attr('disabled','true');
                }

                $(nRow).append( _input_ );
            }
        });

        var table_api_ = data_table_.api();

        /*This is used to handle the on click of our table*/
        table_.on( 'click' , 'tr' , function(){
            if( $(this).has('th').length == 0 ){
                $(this).toggleClass('selected');

                if ( $(this).children('input').attr('disabled') === undefined ){
                    $(this).children('input').attr('disabled','true');
                }else{
                    $(this).children('input').removeAttr('disabled');
                }
            }
        });

        return data_table_;
    },
    "table-edit" : function table ( name , value , data ){

        var table_ = $('<table/>').attr('id','DTT_' + name + '_').addClass("table table-striped table-bordered");
        var table_th_ = $('<thead></thead>');
        var table_tr_ = $('<tr/>');
        var aoColumns_ = [];
        var input_ = $('<input/>').attr('type','text').attr('name',name)
                        .css('display','none').attr('value', JSON.stringify(value));

        for ( var i = 0 ; i < data.length ; i ++ ){
            table_tr_.append( $('<th/>').html( (data[i].text==undefined) ? '' : data[i].text ));
            aoColumns_.push( { 'mData' : data[i].mData } );
        }

        table_.on( 'click' , 'tr' , function(){
            if ( $(this).hasClass('info') ){
                $(this).removeClass('info');
            }else{
                if( $(this).has('th').length == 0 )
                    $(this).addClass('info');
            }
        });

        var data_table_ = table_.append(table_th_.append(table_tr_)).dataTable( {
            "columnDefs" : data,
            "aoColumns" : aoColumns_,
            "aaData" : value
        }).append( input_ );

        table_.on( 'update-table-' + name , function(e){
            //data_table_
        });

        return data_table_;
    }
}