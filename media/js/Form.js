/*Class for creating dynamic forms*/
function Form ( formData ){

    this.method_ = formData.method;
    this.title_ = formData.title;
    this.url_ = formData.url;
    this.fields_ = formData.fields;
    this.submit_ = formData.submit;
    this.dom_ = $("<form></form>").attr('role','form').attr('method',this.method_).attr('action',this.url_).attr('id','form_');

    /*This is our constructor*/
    this.construct__ = function construct__(){

        /*Create our fields*/
        for( var i = 0 ; i < this.fields_.length ; i ++ ){
            this.dom_.append( $('<div/>').addClass('row').append(

                /*Create our label*/
                $('<div/>').addClass('col-md-4').append(
                    $('<h5/>').html( this.fields_[i].label ),//Label Title
                    $('<h6/>').css('color','#999').html( this.fields_[i]['help-text'] )//Help Text
                ),

                /*Create our input type*/
                $('<div/>').addClass('col-md-8 form-group bs-input-middle') .append(
                    FormTypesDesktop_[ this.fields_[i].type ].call(this , this.fields_[i].name , this.fields_[i].value , this.fields_[i].data , this.fields_[i].selected ),
                    /*This is used to create our warning message*/
                    $('<span/>').addClass('help-block label label-danger').css('color','').html('')
                )) , $('<br/>') );
        }

        /*If we dont have a parent then create a form*/
        this.show();
    }

    /*This function will show the form*/
    this.show = function show(){

        var that = this;
        this.form_ = $('<div/>').addClass("modal fade").attr("id","dialog_").attr("role","dialog").attr('tabindex',-1).
                    append( $('<div/>').addClass("modal-dialog").append(
                        $('<div/>').addClass('modal-content').append(

                            /*Add our header*/
                            $('<div/>').addClass('modal-header well well-sm modal-form-header').attr('id','header').append( 
                                $('<button>&times;</button>').addClass('close').attr('type','button').attr('data-dismiss','modal'),
                                $('<h4/>').addClass('').html( this.title_ ) ),

                            /*Add our body*/
                            $('<div/>').addClass('modal-body').append( this.dom_ ),

                            /*Add our submit button*/
                            $('<div/>').addClass('modal-footer').append(
                                    $('<button/>').attr('type','button').addClass('btn btn-primary').html( this.submit_ ).click(
                                            function(){ that.send(); }
                                        )
                                )
                    )));

        $("body").append( this.form_ );
        $('#dialog_').modal('show');
        $('#dialog_').on('hidden.bs.modal' , function(e) { that.destruct__(); } );

        $('#form_').submit(function(e) {
            $(this).ajaxSubmit( {
                dataType : 'json',
                beforeSubmit : that._fnPreSubmit,
                success : that._fnSucess,
                error : that._fnError
            });
            e.preventDefault();
        });

    }

    /*This is our send handler, for sending the form*/
    this.send = function send(){

        $("#form_").submit();
    }

    /*This is our desctrictor*/
    this.destruct__ = function destruct__(){
        this.form_.remove();
    }

    this.construct__();

    this._fnPreSubmit  = function _fnPreSubmit ( formData, jqForm, option ){
        console.log(formData);
    }

    this._fnSucess = function _fnSucess( json ){

    }

    this._fnError = function _fnError ( json ){

    }

}